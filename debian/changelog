clapper (0.6.1-4) unstable; urgency=medium

  * B-D on libsoup-3.0-dev and libmicrodns-dev and enable server

 -- Johannes Schauer Marin Rodrigues <josch@debian.org>  Sat, 20 Jul 2024 08:37:34 +0200

clapper (0.6.1-3) unstable; urgency=medium

  * fixup bug number in last changelog entry

 -- Johannes Schauer Marin Rodrigues <josch@debian.org>  Sat, 13 Jul 2024 02:01:28 +0200

clapper (0.6.1-2) unstable; urgency=medium

  * add (superficial) autopkgtest
  * debian/rules: use --auto-features=enabled (Closes: #1076230)

 -- Johannes Schauer Marin Rodrigues <josch@debian.org>  Sat, 13 Jul 2024 01:55:26 +0200

clapper (0.6.1-1) unstable; urgency=medium

  * New upstream release (closes: #1073926)
  * drop patches
  * adjust lintian overrides for new shared libraries
  * debian/copyright: adjust paths for new source layout
  * debian/control: remove gjs and reword description
  * debian/upstream/metadata: add Donation field

 -- Johannes Schauer Marin Rodrigues <josch@debian.org>  Tue, 09 Jul 2024 07:01:27 +0200

clapper (0.5.2-8) unstable; urgency=medium

  [ Amin Bandali ]
  * Change debian-branch from debian/master to debian/latest

  [ Johannes Schauer Marin Rodrigues ]
  * debian/control: bump Standards-Version (no changes)

 -- Johannes Schauer Marin Rodrigues <josch@debian.org>  Thu, 15 Feb 2024 09:44:24 +0100

clapper (0.5.2-7) unstable; urgency=medium

  * add workaround for https://github.com/Rafostar/clapper/issues/76
    by allowing one to set CLAPPER_DEBIAN_SINGLE_INSTANCE_BREAK_DBUS_MPRIS=1
    as a temporary hack for multiple instances with the side-effect of
    breaking D-Bus and MPRIS support.

 -- Johannes Schauer Marin Rodrigues <josch@debian.org>  Wed, 01 Feb 2023 07:43:33 +0100

clapper (0.5.2-6) unstable; urgency=medium

  * add Depends on gir1.2-soup-3.0 for remote control support

 -- Johannes Schauer Marin Rodrigues <josch@debian.org>  Sun, 29 Jan 2023 20:15:17 +0100

clapper (0.5.2-5) unstable; urgency=medium

  * add patch disabling network access (closes: #1016298)

 -- Johannes Schauer Marin Rodrigues <josch@debian.org>  Sat, 30 Jul 2022 21:23:19 +0200

clapper (0.5.2-4) unstable; urgency=medium

  * debian/control: add missing depends and recommends

 -- Johannes Schauer Marin Rodrigues <josch@debian.org>  Tue, 12 Jul 2022 10:03:25 +0200

clapper (0.5.2-3) unstable; urgency=medium

  * override library-not-linked-against-libc
  * override very-long-line-length-in-source-file
  * override tags related to not splitting out shared library into an extra package
  * debian/salsa-ci.yml: disable cross compilation on salsaci

 -- Johannes Schauer Marin Rodrigues <josch@debian.org>  Sun, 10 Jul 2022 22:57:30 +0200

clapper (0.5.2-2) unstable; urgency=medium

  * source-only upload for testing migration

 -- Johannes Schauer Marin Rodrigues <josch@debian.org>  Fri, 08 Jul 2022 06:40:22 +0200

clapper (0.5.2-1) unstable; urgency=medium

  * Initial release. (Closes: #1003838)

 -- Johannes Schauer Marin Rodrigues <josch@debian.org>  Mon, 27 Jun 2022 17:20:05 +0100
